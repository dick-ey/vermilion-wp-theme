import datetimepicker from 'jquery-datetimepicker';
(function($){

  $(function(){
    // datetime picker
    // being used on events post type

    $('.datetimepicker-field').datetimepicker({
      format:'Y-m-d H:i'
    });
  });

})(jQuery);