<?php

  /**
   * Class Event
   */
  class Event extends \Taco\Post {
    use \Taco\AddMany\Mixins;


    public function getFields() {

      return [
        'start_at'            => [
          'type'        => 'text',
          'description' => '2018-01-01 05:00PM',
          'label'       => 'Start',
          'required'    => true,
          'class'       => 'datetimepicker-field',
        ],
        'end_at'              => [
          'type'        => 'text',
          'description' => '2018-01-01 05:00PM',
          'label'       => 'End',
          'class'       => 'datetimepicker-field',
        ],
        'hashtag'             => [
          'type'        => 'text',
          'pattern'     => '^\#[A-Za-z0-9_]{1,32}$',
          'placeholder' => '#whatsnext',
          'default'     => '#siliconflatirons',
          'style'       => 'width:100%;',
        ],
        'image_path'          => [
          'type'        => 'image',
          'Description' => 'Only used if the event is featured on Home page or Events landing page',
        ],
        'location_name'       => [
          'type'        => 'text',
          'label'       => 'Name',
          'description' => 'Ex: Vermilion Design + Digital',
          'style'       => 'width:100%;',
        ],
        'location_street'     => [
          'type'  => 'text',
          'label' => 'Street Address',
          'style' => 'width:100%;',
        ],
        'location_street2'    => [ 
          'type' => 'text',
          'label' => 'Street Address 2',
          'style' => 'width:100%;'
        ],
        'location_city'       => [
          'type'  => 'text',
          'label' => 'City',
          'style' => 'width:100%;',
        ],
        'location_state_abbr' => [
          'type'      => 'text',
          'label'     => 'State',
          'maxlength' => 2,
          'style'     => 'width:100%;',
        ],
        'location_zip'        => [
          'type' => 'text',
          'label' => 'Zip',
          'style' => 'width:100%;'
        ],
        'location_directions' => [
          'type' => 'textarea',
          'class' => 'wysiwyg',
          'label' => 'Directions'
        ],
        //  'registration_info'  => [
        //    'type'=>'textarea',
        //    'class'=>'wysiwyg',
        //    'label'=>'Additional Info',
        //    'description'=>'Optional supporting registration info'
        //   ],
        //  'sponsored_by_organization_ids' => [
        //    'type'=>'text',
        //    'label'=>'Sponsored By',
        //    'class'=>'addbysearch',
        //    'data-post-type'=>'Organization'
        //   ],
      ];
    }

    /**
     * @param $term_id
     *
     * @return array
     */
    public static function getUpcomingByTermID( $term_id ) {
      $upcoming_events = self::getUpcoming( true );
      if ( ! Arr::iterable( $upcoming_events ) ) {
        return [];
      }

      $upcoming_events = array_filter( $upcoming_events, function ( $event ) use ( $term_id ) {
        return $event->hasTerm( $term_id );
      } );

      return $upcoming_events;
    }

    /**
     * get upcoming events
     * @param int $limit
     * @param bool $load_terms
     * @return array
     */
    public static function getUpcoming( $limit = 5, $load_terms = false ) {
      return self::getBy(
        'end_at',
        date( 'Y-m-d 00:00:00' ),
        '>=',
        [ 'orderby' => 'end_at', 'order' => 'asc',  'posts_per_page' => $limit  ],
        $load_terms
      );
    }

    /**
     * get events that have already passed
     * @param int $limit
     * @param int $offset
     * @param bool $load_terms
     *
     * @return array
     */
    public static function getPrevious( $limit = 50, $offset = 0, $load_terms = false ) {
      return self::getBy(
        'start_at',
        date( 'Y-m-d 00:00:00' ),
        '<',
        [ 'orderby' => 'start_at', 'order' => 'desc', 'posts_per_page' => $limit, 'offset' => $offset ],
        $load_terms
      );
    }

    /**
     * Get the events for the homepage
     *
     * @param int $min
     *
     * @return array
     */
    public static function getForHome( $min = 3 ) {
      $post_id = get_option( 'page_on_front' );
      $page    = ( $post_id ) ? Page::find( $post_id ) : null;

        //      $featured_events = ( $page->featured_event_ids )
        //        ? \AddBySearch\AddBySearch::getPostsFromOrder( $page->featured_event_ids )
        //        : [];

              // If possible, always show at least $min events
        //      if ( $min && count( $featured_events ) < $min ) {
        //        $featured_events = array_combine(
        //          Collection::pluck( $featured_events, 'ID' ),
        //          $featured_events
        //        );
        //
        //        $upcoming_events = Event::getUpcoming();
        //        if ( Arr::iterable( $upcoming_events ) ) {
        //          foreach ( $upcoming_events as $upcoming_event ) {
        //            // Need to make sure no duplicates
        //            if ( array_key_exists( $upcoming_event->ID, $featured_events ) ) {
        //              continue;
        //            }
        //
        //            $featured_events[ $upcoming_event->ID ] = $upcoming_event;
        //          }
        //          $featured_events = array_slice( $featured_events, 0, $min );
        //        }
        //      }

        //      return $featured_events;
        return [ 'this is not working yet' ];
    }

    public function addResourceByID( $resource_id ) {
            //         $resource_ids = explode(',', $this->resource_ids);
      //         $resource_ids[] = $resource_id;
      //         $resource_ids = array_unique($resource_ids);
      //         $this->resource_ids = join(',', $resource_ids);
      return true;
    }

    /**
     * Does this event have related resources?
     * @return bool
     */
    public function hasResources() {
      // return (bool) $this->getResourceIDs();
      return false;
    }

    /**
     * Get the resource IDs
     * @return array
     */
    public function getResourceIDs() {
      // return array_map('intval', array_filter(preg_split('/[^0-9]{1,}/', $this->resource_ids)));
      return [ 'this is not working yet' ];
    }

    /**
     * Get the related resources
     * @return array
     */
    public function getResources() {
      // return \Taco\Post\Factory::createMultiple($this->getResourceIDs());
      return [ 'this is not working yet' ];
    }

    public function getRewrite() {
      return [ 'slug' => 'events' ];
    }

    public function getSupports() {
      return [ 'title', 'editor', 'excerpt', 'revisions' ];
    }

    public function getAdminColumns() {
      return [ 'title', 'start_at', 'location_name' ];
    }

    public function getMetaBoxes() {
      wp_register_script(
        'event_datetimepicker',
        '/wp-content/themes/taco-theme/_/dist/admin.js',
        array( 'jquery' ),
        THEME_VERSION
      );
      wp_register_style( 'event_datetimepicker_css', '/wp-content/themes/taco-theme/node_modules/jquery-datetimepicker/build/jquery.datetimepicker.min.css' );
      wp_enqueue_style( 'event_datetimepicker_css' );
      wp_enqueue_script( 'event_datetimepicker' );

      return [
        'date_and_time'          => [ 'start_at', 'end_at' ],
        'location'               => [ 'location*' ],
        'additional_information' => [ 'image_path' ],
        // 'registration' => ['registration*'],
        // 'sponsored_by' => ['sponsored_by_organization_ids'],
      ];
    }

    /**
     * Get the formatted date
     * @return string
     */
    public function getFormattedDate() {
      $timestamp1 = $this->getStartTimestamp();

      if ( ! $timestamp1 ) {
        return false;
      }

      return ( $this->hasAlreadyHappened() )
        ? date( DATE_FORMAT_LONG, $timestamp1 )
        : date( DATE_FORMAT_LONG, $timestamp1 );
    }

    /**
     * Get the start time
     * @return int
     */
    public function getStartTimestamp() {
      return ( $this->start_at ) ? strtotime( $this->start_at ) : null;
    }

    /**
     * Has this already happened?
     * @return bool
     */
    public function hasAlreadyHappened() {
      //All timestamps will be in terms of Mountain Time, so 21600sec is the offset from UTC.
      if ( $this->isStartTimeSpecific() ) {
        return ( ( $this->getEndTimestamp() + 21600 ) <= time() );
      }

      return ( $this->end_at )
        ? ( strtotime( str_replace( '00:00:00', '23:59:59', $this->end_at ) ) <= time() )
        : true;

    }

    /**
     * Is the start time specific?
     * @return bool
     */
    public function isStartTimeSpecific() {
      return ( $this->start_at && ! preg_match( '/00\:00\:00$/', $this->start_at ) );
    }

    /**
     * Get the end time
     * @return int
     */
    public function getEndTimestamp() {
      return ( $this->end_at ) ? strtotime( $this->end_at ) : null;
    }

    /**
     * Get the formatted datetime
     *
     * @param string $replace_spaces_with
     *
     * @return string
     */
    public function getFormattedDateTime( $replace_spaces_with = ' ' ) {
      $timestamp1 = $this->getStartTimestamp();
      $timestamp2 = $this->getEndTimestamp();

      // check days
      $firstDate  = date( 'Y-m-d', $timestamp1 );
      $secondDate = date( 'Y-m-d', $timestamp2 );

      if ( ! $timestamp1 ) {
        return false;
      }

      $date = date( DATETIME_FORMAT_LONG, $timestamp1 );

      if ( $timestamp2 ) {
        // is same day?
        if ( $firstDate != $secondDate ) {
          $date = $date . ' - ' . date( DATETIME_FORMAT_LONG, $timestamp2 );
        } else {
          $date = $date . ' - ' . date( 'g:ia', $timestamp2 );
        }
      }

      return $date;
    }

    /**
     * Is there a location set?
     * @return bool
     */
    public function hasLocation() {
      return Arr::iterable( $this->getLocationSegments() );
    }

    /**
     * Get the location segments that we care about for most displays
     * @return array
     */
    public function getLocationSegments() {
      return array_filter( [
        $this->location_name,
        $this->location_street,
        $this->location_street2,
        $this->location_city,
        $this->location_state_abbr,
      ] );
    }

    /**
     * Does the event span across multiple days?
     * @return bool
     */
    public function isMultiday() {
      $timespan = $this->getEndTimestamp() - $this->getStartTimestamp();

      // 86400 seconds in 24hrs.
      return ( $timespan >= 86400 ) ? true : false;

    }

    /**
     * Get the formatted location
     *
     * @param string $prefix
     * @param bool $with_google_maps_link
     *
     * @return string
     */
    public function getFormattedLocation( $prefix = '@ ', $with_google_maps_link = true ) {
      $segments = $this->getLocationSegments();
      $name     = ( Arr::iterable( $segments ) )
        ? sprintf( '%s%s', $prefix, join( ', ', $segments ) )
        : null;
      if ( ! $name ) {
        return $name;
      }

      if ( ! $with_google_maps_link ) {
        return $name;
      }

      $google_maps_url = $this->getGoogleMapsURL();

      return ( $google_maps_url )
        ? sprintf( '<a target= _blank" href="%s">%s</a>', $google_maps_url, $name )
        : $name;
    }

    /**
     * Get the Google Maps URL
     * @return string
     */
    public function getGoogleMapsURL() {
      $location_segments = array_filter( array_map( 'trim', [
        $this->location_name,
        $this->location_street,
        $this->location_street_2,
        $this->location_city,
        $this->location_state_abbr,
        $this->location_zip,
      ] ) );
      if ( count( $location_segments ) >= 4 ) {
        $location_segments = array_map( function ( $segment ) {
          return preg_replace( '/[\s]{1,}/', '+', $segment );
        }, $location_segments );

        return sprintf(
          'https://maps.google.com?q=%s',
          join( '+', $location_segments )
        );
      }

      return null;
    }

    /**
     * determine if an event has sponsors
     * @return bool
     */
    public function hasSponsoredByOrganizations() {
      return Arr::iterable( $this->getSponsoredByOrganizationIDs() );
    }

    /**
     * This is not setup to work yet.
     * @return array
     */
    public function getSponsoredByOrganizationIDs() {
      return array_unique( array_filter( preg_split( '/[^0-9]/', $this->sponsored_by_organization_ids ) ) );
    }

    /**
     * this is not setup to work yet
     * @return array
     */
    public function getSponsoredByOrganizations() {
      $post_ids = $this->getSponsoredByOrganizationIDs();

      return ( Arr::iterable( $post_ids ) )
        ? \Taco\Post\Factory::createMultiple( $post_ids )
        : [];
    }
  }
