<?php

  class Person extends \Taco\Post {
    use \Taco\AddMany\Mixins;

    public function getFields() {
      return [
        'first_name' => [
          'type'  => 'text',
          'style' => 'width: 100%',
        ],

        'last_name' => [
          'type'  => 'text',
          'style' => 'width: 100%',
        ],

        'photo' => [
          'type' => 'image',
        ],

        'role' => [
          'type'  => 'text',
          'style' => 'width: 100%',
        ],

        'bio' => [
          'type'  => 'textarea',
          'class' => 'wysiwyg',
          'style' => 'width: 100%',
        ],

        'is_featured_person' => [
          'type' => 'checkbox',
        ],

      ];

    }

    public function getSingular(){
      return 'Person';
    }


    public function getPlural() {
      return 'People';
    }

    public function getRewrite() {
      return [ 'slug' => 'people' ];
    }

    public function getSupports() {
      return [ 'title', 'editor', 'excerpt', 'revisions' ];
    }

    public function getDefaultOrderBy() {
      return 'last_name';
    }

    public function getDefaultOrder() {
      return 'DESC';
    }

    public function getAdminColumns() {
      return [ 'title', 'role', 'is_featured_person' ];
    }

    public function getFullName() {
      return $this->first_name . ' ' . $this->last_name;
    }

    public function getPhoto() {
      return !empty($this->photo) ? $this->photo : get_asset_path('img/portrait_placeholder.jpg');
    }

  }