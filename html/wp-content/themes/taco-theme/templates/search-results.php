<?php /* Template Name: Page - Search Results */ ?>

<?php
  get_header();
  $page = \Taco\Post\Factory::create($post);
?>

<?php
  if (isset($_GET['keyword'])) {
    // Args get escaped in the getSearchResults function
    $post_type  = !empty($_GET['posttype']) ? $_GET['posttype'] : '';
    $current_page = !empty($_GET['pagenum']) ? (int) $_GET['pagenum'] : 1;
    $order = !empty($_GET['order']) ? $_GET['order'] : 'desc';
    $orderby = !empty($_GET['orderby']) ? $_GET['orderby'] : 'score';
    $keyword = $_GET['keyword'];

    $args = [
      'offset'    => ($current_page - 1) * 5,
      'post_type' => $post_type,
      'orderby'   => $orderby,
      'order'     => $order,
    ];

    // Use this for the PHP call version. use getJSONSearchResults for AJAX.
    $search_results = SiteWideSearchInstance::getSearchResults($keyword, $args);
  } else {
    $search_results = json_encode([]);
  }
?>

<script>
  var initial_search_results = <?=$search_results?>;
  var items_per_page = <?=5;?>;
  var current_page = <?=$current_page?>;
  var order = <?=json_encode($order)?>;
  var orderby = <?=json_encode($orderby)?>;
</script>

<main class="first-panel">
  <h1 class="center">Search</h1>
    <div class="row columns">
    <form action="/search-results" method="GET">
      <input type="text" name="keyword">
      <input type="submit" value="Search">
    </form>

      <div id="search-results" is="search-results">
        
      <?php  if (Arr::iterable($search_results)) : ?>
        <h2>Results</h2>
      <ul>
        <?php foreach ($search_results as $result): ?>
        <li>
          <h3>
            <a href="<?= $result->getPermalink() ?>"><?= $result->getTheTitle(); ?></a>
          </h3>
          <p><?= $result->getTheExcerpt() ?></p>
        </li>
        <?php endforeach ?>
      </ul>

      <?php endif?>
      </div>
    </div>
</main>

<?php get_footer(); ?>