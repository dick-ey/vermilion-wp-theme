<?php
require_once __DIR__ . '/core/SiteWideSearch.php';

class SiteWideSearchInstance extends SiteWideSearch {

  // return a multi-dimensional array containing keys of post types and values of field => points
  // leaving the key empty will assume overriding default fields
  public static function getSearchableFields() {
    return array(
      'page' => array(
        'post_content' => 1,
        'post_title' => 4,
      ),
      'post' => array(
        'post_title' => 4,
        'post_content' => 1,
      )

    );
  }

  public static function getJSONFields($results) {
    $results = array_map(function($result) {
      switch ($result['post_type']) {
        case 'page':
        case 'board-member':
        case 'team-member':
        case 'fellow':
        case 'leader':
          $result['post_date'] = '';
          break;
      }

      return $result;
    }, parent::getJSONFields($results));

    return $results;
  }
}
